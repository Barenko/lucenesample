package searcher;

import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

public interface Searcher {
	Searcher useIndex(Directory index);
	Searcher useAnalyzer(Analyzer analyzer);
	Searcher useVersion(Version luceneVersion);
	Searcher addToIndex(String... contents);
	Searcher addToIndex(Set<String> contents);
	List<String> search(String query, int hits);
}
