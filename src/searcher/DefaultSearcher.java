package searcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

public class DefaultSearcher implements Searcher {

	private Version luceneVersion = Version.LUCENE_36;
	private Directory index = new RAMDirectory();
	private Analyzer analyzer = new StandardAnalyzer(luceneVersion);
	private IndexWriter indexWriter = null;
	private String field;
	
	public DefaultSearcher(){
		this("field");
	}
	
	public DefaultSearcher(String field){
		this.field = field;
	}

	@Override
	public Searcher useIndex(Directory index) {
		this.index = index;
		return this;
	}

	@Override
	public Searcher useAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
		return this;
	}

	@Override
	public Searcher useVersion(Version luceneVersion) {
		this.luceneVersion = luceneVersion;
		return this;
	}

	@Override
	public Searcher addToIndex(String... contents) {
		for (String c : contents) {
			Document doc = new Document();
			doc.add(new Field(field, c, Field.Store.YES, Field.Index.ANALYZED));
			addToIndexWriter(doc);
		}
		try {
			indexWriter.commit();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return this;
	}

	protected void addToIndexWriter(Document doc) {
		try {
			if (indexWriter == null) {
				IndexWriterConfig config = new IndexWriterConfig(luceneVersion, analyzer);
				indexWriter = new IndexWriter(index, config);
			}
			indexWriter.addDocument(doc);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Searcher addToIndex(Set<String> contents) {
		addToIndex(contents.toArray(new String[] {}));
		return this;
	}

	@Override
	public List<String> search(String query, int hits) {
		try {
			Query q = new QueryParser(luceneVersion, field, analyzer).parse(query);
			IndexReader reader = IndexReader.open(index);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopScoreDocCollector collector = TopScoreDocCollector.create(hits, true);
			searcher.search(q, collector);
			ScoreDoc[] scores = collector.topDocs().scoreDocs;
			List<String> results = new ArrayList<String>(scores.length);
			for (int i = 0; i < scores.length; i++) {
				results.add(searcher.doc(scores[i].doc).get(field));
			}
			searcher.close();
			return results;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
